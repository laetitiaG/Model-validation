from typing import Dict, List
from DataStorage import *


phenotype_table ={'TDS-0':lambda x: "AF(AG("+x+"=0))",
                  'TDS-1':lambda x: "AF(AG("+x+"=1))",
                  'TDS-2':lambda x: "AF(AG("+x+"=2))",
                  '!TDS-0':lambda x: "!(AF(AG("+x+"=0)))",
                  '!TDS-1':lambda x: "!(AF(AG("+x+"=1)))",
                  '!TDS-2':lambda x: "!(AF(AG("+x+"=2)))",
                  'OSC-1': lambda x: "AG(AF("+ x + "=0) & AF(" + x + "=1 ))",
                  'OSC-2': lambda x: "AG(AF(" + x + "=0) & AF(" + x + "=1 )) | AG(AF(" + x + "=0) & AF(" + x + '=2 )) | AG(AF(' + x + "=2) & AF(" + x + "=1 ))",
                  'OSC-3': lambda x: "AG(AF("+ x + "=0) & AF(" + x + ">=1 )) | AG(AF("+ x + ">=0) & AF(" + x + "=2 )) | AG(AF("+ x + "=2) & AF(" + x + "=1 )) | AG(AF("+ x + "=0) & AF(" + x + "=3 )) | AG(AF("+ x + "=3) & AF(" + x + "=2 )) | AG(AF("+ x + "=3) & AF(" + x + "=1 ))",
                  '!OSC-1': lambda x: "!(AG(AF("+ x + "=0) & AF(" + x + "=1 )))",
                  '!OSC-2': lambda x: "(AG(AF(" + x + "=0) & AF(" + x + "=1 )) | AG(AF(" + x + "=0) & AF(" + x + '=2 )) | AG(AF(' + x + "=2) & AF(" + x + "=1 )))",
                  'osc': lambda x,mi,ma: "AG(AF("+ x + "=" + mi + ") & AF(" + x + "=" + ma + ")) & AF(AG("+ x +" <= " + ma + " & "+ x + " >=" + mi + ")) ", #todo: attention ici changmeent de la formule pour exclure les autres
                  #'osc': lambda x,mi,ma: "AG(AF("+ x + "=" + mi + ") & AF(" + x + "=" + ma + "))",
                  '1-2': lambda x,y: x + " =1 -> AF(AG(" + y + "=2 ))",
                  '1-0': lambda x,y: "AF(AG(" + x + "=1)) -> AF(AG(" + y + "= 0))",
                '0-1': lambda x,y: "AF(AG(" + x + "=0)) -> AF(AG(" + y + "= 1))",
                'NR-0':lambda x: "AG(!"+x+"=0)",
                'NR-1':lambda x: "AG(!"+x+"=1)",
                'NR-2':lambda x: "AG(!"+x+"=2)",
                }


def getCTL(key,var, line, col):
    """
   return the FAIR-CTRL translation to the key word given
    :param key: give the key word to find the associated formula in STR
    :param var: give the associated variable for the phenotype in STR
    :param line: give the line where the key is in the matrix
    :para: col: give the colon where the key is in the matrix
    :return: return FAIR-CTRL formulas in STR
    """
    try:
        lamb = phenotype_table[key]
        return str(lamb(*var))

    except:
        print("Warning: Key '"+ str(key) + "' for the variable "+ str(var) +" in line:"+str(line)+ " col:" + str(col) + " of the matrix does not exist \n No formula is added for this line")
        return(False)


def CTL_wt_one_var(s_var, phenotype):
    """
    it reads a cell in matrix column where there is only one variable involved. var is a list to put un get CTL function to translate to a correct FAIRCTL formula
    :param s_var: is a string  representing the variable. it represent  the noun of the column
    :param phenotype: is a phenotype (it represent a cell of the validation matxix)
    :return: key, var  return a list of variable
    """
    var = [s_var]
    if 'osc' in phenotype:
        # phenotype_line [i] is the cell in colomn i+1 on the phenotype matrix
        key = phenotype[:-5]
        #print(phenotype)
        var.append(phenotype[4])
        var.append(phenotype[6])
        #print(key, var)
    else:
        key = phenotype
    return key, var

def CTL_wt_one_cell(l_cell_key, l_var_phenotype_name_wt_bound, line, col):
    #changer ici renvoyer liste sépraré des types de
    """
    it reads a cell in matrix column where there is only one variable involved. var is a list to put un get CTL function to translate to a correct FAIRCTL formula
    :param l_cell_key: is a string with the content of the cell to TRANSLATE
    :param  l_var_phenotype_name_wt_bound: contain the bounds of that the vairable can take
    :param  line: line of the cell for the validation matrix
    :param: col: column of the cell for the validation matrix
    :return: CTL cell, str formula : return the CTL(s) formula corresponding to the cell comportement
    """
    l_CTL_cell = []
    num_f_cell = 1
    formula_key = []
    #conditon FAIR-CTL(etiquette)
    for e in l_cell_key:# ici avec la focntion pour prédction peut avoir plusieurs formules a tester dans une case
        if e != "":
            if len(l_var_phenotype_name_wt_bound) > 1: #todo:creer des conditions et adapter focntion avec nouveaux phénotypes
                key = e
                var = l_var_phenotype_name_wt_bound
            else:
                #print(l_var_phenotype_name_wt_bound[0])
                #print(e)
                key, var = CTL_wt_one_var(l_var_phenotype_name_wt_bound[0], e)
            FAIRCTL = getCTL(key, var, line, col)  # type: str
#            FAIRCTL = getCTL(key, var)
            #print("in one cell :CTL")
            #print(CTL)
            formula_key.append("FC"+ str(line) + str(col) + str(num_f_cell))
            if FAIRCTL != (False):
                l_CTL_cell = l_CTL_cell + ['FC' + str(line) + str(col) + str(num_f_cell) + ' = (' + FAIRCTL + ')']
            #print("in one cell : CTL_CELL")
            #print(l_CTL_cell)
            num_f_cell += 1
    CTL_cell = " ; ".join(l_CTL_cell)
    str_formula = " ".join(formula_key)
    return CTL_cell, str_formula

def macro_handle2(var_phenotype_names, phenotype_line, env_line):
    #TODO: faire fonction pour indiquer  si CTL ou faire CTL
    """
    it make the translation to V-PM information to CTL for a line merging all validation process.
    :param var_phenotype_names: is a matrix line containing list phenotype var names
    :param phenotype_line: is a matrix line containing a list of phenotypes expected.
    :return CTL_line: CTl in str for a matrix line corresponding to one smb file
    """
    if len(var_phenotype_names) != len(phenotype_line):
        #todo:gerer l'erreur mieux que ça, renvoyer erreur matrice
        return 'ERROR: wrong matrix writing in the phenotype part', 0
    else:
        l_CTL_line = []
        CTL_keys = []
        col = 0
        while col < len(var_phenotype_names):
            var_phenotype_names_wo_sp = var_phenotype_names[col].replace(' ', '')
            l_var_phenotype_name_wt_bound = var_phenotype_names_wo_sp.split('_')
            phenotype_cell = phenotype_line[col].split('_')
            # split to have all variables in the column in a list
            CTL_cell, CTL_key = CTL_wt_one_cell(phenotype_cell, l_var_phenotype_name_wt_bound, env_line, col+1)
            #print("CTL_cell: macro hundle2")
            #print(CTL_cell+"b")
            if CTL_cell != "":
                l_CTL_line = l_CTL_line + [CTL_cell]  # type: list
                CTL_keys = CTL_keys + [CTL_key]
            col += 1
        CTL_final = " ; ".join(l_CTL_line)
        if CTL_final != "":
            CTL_final = CTL_final
        return CTL_final, CTL_keys

def phenotype_translation_CTL(phe_dpm): #use only this function to create premisse with
    """
    :param phe_dpm:  with line of the V-PM as key and content of each line , and with phenotype cells in a list
    :param l_var: list of variables of the model and their bounds ['a=0..1;'b=0..2;']
    :return: d_CTL: dict with line of the V-PM as key and CTL formula associated to phenotype in str as value
    """
    d_CTL = {}  # type: Dict[int, str]
    d_CTL_ID = {}
    var_phenotype_names = phe_dpm[0]  # type: list
    line = 1
    #print(len(phe_dpm))
    while line < len(phe_dpm):
        y,x = macro_handle2(var_phenotype_names, phe_dpm[line], line)
        d_CTL[line] = y
        d_CTL_ID[line] = x
        line += 1
        #todo:ajouter possible CTL

    return d_CTL, d_CTL_ID

def bloc_init_creation(d_init_var):#todo: use the init bloc to do smb files
    """
    it translate V-PM init-var information to premise which need to be used in smb files
    :param d_init_var: dict with line of the V-PM as key and content of each line for init-vars of the V-PM matrix  in a list as value
    :return: d_bloc_var: dict with line of the V-PM as key and has fomulas to put in  init bloc associated
    """
    if d_init_var != {}:
        d_bloc_var = {0: 'INIT\n'}
        line = 1
        while line <= len(d_init_var):
            if line != 'v':
                 d_bloc_var[line] = d_init_var[0] + ' = ' + d_init_var[line] + ';\n'
            line += 1
    return(d_bloc_var)


def init_var_translation_to_premise(d_init_var):  # todo: if not init var or fixed var pas faire this function + pas faire fonction avec les INIT
    """
    it translate V-PM init-var information to premise which need to be used in smb files
    :param d_init_var: dict with line of the V-PM as key and content of each line for init-vars of the V-PM matrix  in a list as value
    :return: d_premise: dict with line of the V-PM as key and premise formula associated  to biological context init_var in a list  as value
    """
    d_premises = {}  # type: Dict[int, list]
    if len(d_init_var) != 0:
        line = 1
        init_var_names = d_init_var[0]
        while line < len(d_init_var):
            if line in d_init_var:
                #print(line)
                # if there is no the  key line in the dict it seems that there is "v" in each Fix and init var columns (if they exists)
                l_value_init_var = d_init_var[line]
                # value of init var for one biological context
                premise = ''  # type: str
                # premise for a line
                i = 0
                while i < len(l_value_init_var):
                    # while we are not at the end of the list of init var value for the line of the matrix
                    if l_value_init_var[i] != 'v':
                        # if 'v'  is  in line of init var the var in not an init in thi biological context
                        premise = premise + init_var_names[i] + '=' + l_value_init_var[i] + '&'
                        premise = premise[0:-1]
                        # TODO: construction verification
                        # beginning of the premise and ad new init condition var  by adding the 'name = value '
                        #print(premise)
                        d_premises[line] = premise
                    i += 1
            line += 1
    return d_premises

#this function does not need to be launched if  we use the para bloc in TOtembionet
def add_premise_CTL(d_CTL, d_premise): #TODO fonction inutile avec le bloc init
    """
    it merge premise and CTL of each line of the V-PM matrix to form CTL bloc for each future smb file
    :param d_CTL: dict with line of the V-PM as key and CTL formula associated to phenotype in a str as value
    :param d_premise: dict with line of the V-PM as key and premise formula associated  to biological context init_var in a list as value
    :return: d_smbs_complete_CTL : dict with line of the V-PM as key and logical formula associated  to complete CTL bloc of a biological context (smb) in str as value
    """
    d_smbs_complete_ctl = {}  # type: List[str]
    if len(d_premise) == 0:
        # if there is no init var
        return d_CTL
    else:
        line = 1
        while line <= len(d_CTL):
            #print(line)
            #print(d_premise.keys())
            if line in d_premise.keys(): #pas bon
                # if there is no init var for the line
                #print(d_premise[line])
                #print(d_CTL[line])
                smb_complete_ctl = d_premise[line] + '->' + d_CTL[line]  # type: str
                #print(d_smbs_complete_ctl)
                d_smbs_complete_ctl[line] = smb_complete_ctl
            else:
                #print('b')
                d_smbs_complete_ctl[line] = d_CTL[line]
            line += 1
    return d_smbs_complete_ctl

