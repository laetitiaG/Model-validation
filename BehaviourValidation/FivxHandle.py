import sys, os

from typing import Dict, List

sys.path.append(os.path.abspath("."))
from BehaviourValidation.DataStorage import ps_data_storage, vpm_data_storage, b_context_separation

sys.path.append(os.path.abspath("BehaviourValidation"))
sys.path.append(os.path.abspath(""))


# TODO: gestion erreur dans cas ou pas des fixed dans tous les lignes mais que certaines ... certains elements de dicos vides...


def para_for_one_fixed_var(fix_var_name, fix_var_value,
                           l_para):  # probleme sur la formation des para pour les fixed var
    """
    it create a para for one fixed var
    :param fix_var_name: str name of a fixed var
    :param fix_var_value: str of a fixed value corresponding to its name for one fixed var
    :param l_para: parameter list of PS
    :return: para_wt_fix_ress ; str of all parameter which need to be added for one fixed var
    """
    # print('fix_var_name:' + fix_var_name)
    para_to_find = 'K_' + fix_var_name + ':'
    para_to_find2 = 'K_' + fix_var_name + '='
    para_wt_fix_ress = ''
    for para_wt_value_sp in l_para:
        para_wt_value = para_wt_value_sp.replace(" ", "")
        if para_to_find in para_wt_value or para_to_find2 in para_wt_value:
            l_para_wt_value = para_wt_value.split('=')
            # print(l_para_wt_value[0])
            para_wt_fix_ress = para_wt_fix_ress + l_para_wt_value[
                0] + ':In' + fix_var_name + '_env =' + fix_var_value + ';\n'
            # para_wt_fix_ress for para with ressource the fixed variable
        # todo gerer erreur si parametre pas trouvé.. normalement pas besoin car verifie autre fichier que ig et para correspondent (helene)
    return para_wt_fix_ress


#todo: trouver une automatisation possible pour les oscillations avec cette fonction
def para_for_one_osc_var():
    pass


def para_for_fixed_var_line(l_fix_var_name, l_fix_var_value, l_para):
    # TODO: si pas de fixed var pas faire cette fonction pas lancer (dans main)
    """
    it create a list of para for fixed var for a line of the matrix(one smb file)
    :param l_fix_var_name: list of fixed var names of the V-PM
    :param l_fix_var_value: list of fixed value corresponding to their names for one line of the V-PM
    :param l_para: parameter list of PS
    :return: l_para_wt_fix_ress ; list of all parameter which need to be added to one smb file
    corresponding to one line of the V-PM  biological context to configure.
    """
    l_para_wt_fix_res = []
    i = 0
    while i < len(l_fix_var_name):
        fix_name = l_fix_var_name[i]
        fix_value = l_fix_var_value[i]
        if fix_value != 'v':
            para_res = para_for_one_fixed_var(fix_name, fix_value, l_para)
            l_para_wt_fix_res.append(para_res)

        i += 1
    return l_para_wt_fix_res


# function ok


def line_fixed_var_handling(l_fix_var_name, l_fix_var_value, l_para_wt_fix_ress):
    """
    it make a dit for handle fixed var in one smb file (one line of the matrix)
    :param l_fix_var_name: list of fixed var names of the V-PM
    :param l_fix_var_value: list of fixed var values of the V-PM
    :param l_para_wt_fix_ress: list of all parameter wich need to be added to one smb file
    corresponding to one line of the V-PM  biological context to configure.
    :return: d_smb_fix_var: dict of handled fixed var  in list information f a V-PM line
    """
    d_smb_fix_var = {}  # type: Dict[int, List[str]]
    l_fix_envar_line = []
    l_fix_reg_line = []
    i = 0
    while i < len(l_fix_var_name):
        envar = 'In' + l_fix_var_name[i]
        l_fix_reg_line.append('In' + l_fix_var_name[i] + '_env [' + envar + ' >=1 ] =>' + l_fix_var_name[i] + ';\n') #add line to regulation block
        if l_fix_var_value[i] != 'v':
            l_fix_envar_line.append(envar + '= 1 ;\n') #add line to ENVAR block  1 if we want to fix the vairable and 0 if we don't want
        else:
            l_fix_envar_line.append(envar + '= 0 ;\n')
        i += 1
        # TODO: verify writing of para for one fixed
    if l_fix_envar_line:
        d_smb_fix_var['env'] = l_fix_envar_line
        d_smb_fix_var['para'] = l_para_wt_fix_ress
        d_smb_fix_var['reg'] = l_fix_reg_line
    return d_smb_fix_var


def convert_fixed_var_dict_list_to_str(d_smb_fix_var):
    """
    convert values of the dict in str
    :param d_smb_fix_var:
    :return: d_smb_fix_var_str: dict of handled fixed  in str var information f a V-PM line
    """
    d_smb_fix_var_str = {}
    for li, v in d_smb_fix_var.items():
        # in the dict there values are dict btu we want str
        str_li = "".join(v)
        d_smb_fix_var_str[li] = str_li  # type: Dict[str,str]
    return d_smb_fix_var_str


# ok

def fixed_var_to_all_vpm(d_FixVar, l_para):  # todo:revoir la focntion et son but et faire après para
    """
    it make a unique structure for all matrix fixed var handling.
    :param d_FixVar: dict of each line for fixed var
    :return:d_smbs_fixed_var: dict with line as keys and str as
    """
    d_smbs_fixed_var = {}  # type: Dict[int,dict[str,list[str]]]
    if d_FixVar != {}:
        # print('d_FIXED:')
        # print(d_FixVar)
        l_fix_var_name = d_FixVar[0]
        line = 1
        # print(d_FixVar[line])
        while line < len(d_FixVar):
            # exist some line where there is no fixed var so donc create element in d_smbs_fixed_var
            l_para_wt_fix_ress = para_for_fixed_var_line(l_fix_var_name, d_FixVar[line], l_para)
            d_smb_fix_var = line_fixed_var_handling(l_fix_var_name, d_FixVar[line], l_para_wt_fix_ress)
            if d_smb_fix_var:  # if the matrix line does not have any fixed var
                d_smb_fix_var_instr = convert_fixed_var_dict_list_to_str(d_smb_fix_var)
                d_smbs_fixed_var[line] = d_smb_fix_var_instr
            line += 1
    return d_smbs_fixed_var


def envar_to_all_vpm(d_envar, d_smbs_fix_var):
    """
    its create the dict where all envar are assigned to its value  according the line matrix. it correspond to a dit of
    envar for smbs files , it use also fixed var
    :type d_smbs_fix_var:
    :param d_envar: dict of envar list for each line
    :return: d_smbs_envar : dict with all envars ranked by the corresponding line number in V-PM ( an elment for a smb file)
    """
    d_smbs_envar = {}
    d_envar_names = d_envar[0]
    # print(d_envar_names)
    #smb_envar = ''
    line = 1
    while line < len(d_envar):
        smb_envar_values = d_envar[line]
        #print("values-var: ")
        #print(smb_envar_values)
        # print(smb_envar_values)
        col = 0
        smb_envar = ''
        while col < len(d_envar[line]):
            smb_envar = smb_envar + d_envar_names[col] + '=' + smb_envar_values[col] + ';\n'
            # print(smb_envar)
            col += 1
        if line in d_smbs_fix_var:
            smb_fixed_var = d_smbs_fix_var[line]['env']
            # print(smb_fixed_var)
            d_smbs_envar[line] = smb_envar + smb_fixed_var
        else:
            d_smbs_envar[line] = smb_envar
        line += 1
    #print(d_smbs_envar)
    return d_smbs_envar

###############----------------tests-------------------############

# test file functions with structures

# d_fix_var = {0: ["a", "b", "c"], 1: ["1", "v", "2"], 2: ["0", "1", "v"]}
# TESTl_fix_var_name = ["a", "b", "c"]
# TESTl_fix_var_value = ["1", "v", "2"]
# l_para = ['K_operon = 0', 'K_operon:alg = 0', 'K_operon:free = 0..2', 'K_operon:alg:free = 0..2', 'K_mucuB = 0',
#         'K_mucuB:prod : 0', 'K_mucuB:Inmucus = 0', 'K_mucuB:prod:Inmucus = 0..1']
# l_fix_var_name = ['mucuB', 'operon']
# l_fix_var_value = ["1", "v"]
# l_fix_var_value2 = ["v", "2"]
# d_FixVar = {0: ['mucuB', 'operon'], 1: ["1", "v"], 2: ["v", "2"]}
# fix_var_name = "operon"
# fix_var_value = "1"


# VPM2 = vpm_data_storage("./../modelingcontext/V_PM2.csv", "V-PM")
# l_para = ps_data_storage("./../modelingcontext/PARA.smb")
# d_fix, d_envar, d_init = b_context_separation(VPM2['context'])
# print(l_para)

# print(l_para)
# para_for_one_fixed_var_test = para_for_one_fixed_var(fix_var_name, fix_var_value, l_para)
# print(para_for_one_fixed_var_test)
# l_para_wt_fix_ress_test = para_for_fixed_var_line(l_fix_var_name, l_fix_var_value, l_para)
# print(l_para_wt_fix_ress_test)

# l_para_wt_fix_ress_test2 = para_for_fixed_var_line(l_fix_var_name, l_fix_var_value2, l_para)
# print(l_para_wt_fix_ress_test2)
####==>OK

# d_smb_fix_var = line_fixed_var_handling(l_fix_var_name, l_fix_var_value, l_para_wt_fix_ress_test)

# d_smb_fix_var_str = convert_fixed_var_dict_list_to_str(d_smb_fix_var)

# ok


# d_smbs_fixed_var = fixed_var_to_all_vpm(d_fix, l_para)
# print(d_smbs_fixed_var)
# {2: {'env': 'Inoperon=1;\n', 'para': 'K_operon:Inoperon=1;\nK_operon:alg:Inoperon=1;\nK_operon:free:Inoperon=1;\nK_operon:alg:free:Inoperon=1;\n', 'reg': '[Inoperon >=1 ] =>operon;\n'},


# d_envar = {0: ['test','test2'], 1: ['0','1'], 2: ['1','2']}
# envar_with_fix = envar_to_all_vpm(d_envar, d_smbs_fixed_var)
# print(envar_with_fix)
# OK

# to Handle fixed var, use fixed_var_to_all_vpm and envar_to_all function needed to be lunched, if there is no fixed var don't lunch it because d_fix_var is empty


####################################################################################################
###################################### unitary test strctures################################################

# d_smb_fix_var = {"env": ["Ina=1;\n", "Inc=2\n;"], "reg": ["[Ina >= 1] => a ;", "[Inc >= 1] => c ;"],
#                  "para": ['K_a= 0 ;\nK_a:Ina =1 ;\n', 'K_c= 0 ;\nK_c:Inc =1 ;\n']}


# d_smb_fix_var_str = {"env": "Ina=1; \nInc=2;", "reg": "[Ina >= 1] => a ; \n[Inc >= 1] => c ;",
#                    "para": 'K_a= 0 ;\nK_a:Ina =1 ;\nK_c= 0 ;\n K_c:Inc =2 ;\n'}


# d_smbs_fixed_var = {1: {"env": "Ina=1; \nInc=2;", "reg": "[Ina >= 1] => a ; \n[Inc >= 1] => c ;",
#                       "para": 'K_a= 0 ;\nK_a:Ina=1 ;\nK_c= 0 ;\n K_c:Inc =2 ;\n'},
#                    2: {"env": "Ina=0; \nInb=1;", "reg": "[Ina >= 1] => a ; \n[Inb >= 1] => b ;",
#                      "para": 'K_a= 0 ;\nK_a:Ina=1 ;\nK_c= 0 ;\n K_b:Inb =1 ;\n'}}
