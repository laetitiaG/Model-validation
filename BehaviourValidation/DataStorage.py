import os
import subprocess
from typing import List, Dict


# print(os.environ['TOTEM_PATH'])

# todo: écire function data storage, corriger et fini fichier handling et faire exemple avec structures.


def read_file(file):

    try:
        with open(file) as f:
            file_by_line = f.read().splitlines()  # read each line which are stored in a list, delete \n from CSV input file.
            # print((file_by_line))
    except:
        raise FileNotFoundError(
            "Input file doesn't exist in your file system. Please, rename it, move it or create one.")
        # dans ce cas là, la dernière exception générée n'est pas FileNotFound. Je lance une exception de ce type car "fichier introuvable" est l'exception causale dans ce try.
    else:
        f.close()
    return file_by_line


def smb_data_storage(path_to_intput_file):
    """
    it stores ig information in dict and para information from a smb input file
    :param path_to_intput_file: file in .smb
    :return: d_IG: dict of VAR reg and envar  in list usable for input files of totembionet
    :return: l_para: list of paramter stored in STR usable for input files of totembionet
    """
    d_ig = {}
    smb_per_line = read_file(path_to_intput_file)
    var = []
    reg = []
    l_para = []
    i_REG = int(smb_per_line.index("REG"))
    i_PARA = int(smb_per_line.index("PARA"))
    i_VAR = int(smb_per_line.index("VAR"))
    for i in range(i_VAR + 1, i_REG):
        # print(e)
        var = var + [smb_per_line[i]]  # print(var)
    for e in range(i_REG + 1, i_PARA):
        reg = reg + [smb_per_line[e]]
    for e in range(i_PARA + 1, len(smb_per_line) - 1):
        l_para = l_para + [smb_per_line[e]]
    #print(l_para)
    d_ig['var'] = var
    d_ig['reg'] = reg
    return (l_para, d_ig)


def run_Totembionet(option, path, pathcsv=None):
    if pathcsv is None:
        p = subprocess.Popen(['totembionet', option, path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    else:
        p = subprocess.Popen(['totembionet', path, option, pathcsv, '-csv', '-verbose', '3'], stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
    # std_out, std_err = p.communicate()
    # print(std_err)
    return p.stdout.read()  # , #std_err


def ig_data_storage(IG):
    """
    it stores ig information in dict
    :param IG: file in graphml
    :return: d_IG: dict of VAR reg and envar  in list
    """
    d_IG = {}
    l_Var = []
    l_Reg = []
    l_envar = []
    current_path = os.getcwd()
    # print(current_path) #attention current path différent en focntion de l'endroit ou on lance python3 pour la commande
    # todo:gerer l'endroit du main pour ne pas avoir de disjonction entre les deux pas avoir une commande relative au lancement mais chemain global et penser a ajouter/
    path = current_path + '/modelingcontext' + '/' + IG
    # print('path :' + path)
    file_IG = path[:-8] + 'FromYed.smb'
    # print(file_IG)
    if os.path.exists(file_IG):
        os.remove(file_IG)
    a, b = run_Totembionet("-yed", path)  # todo: erreur process corriger..;
    # print(b)
    IG_file_by_line = read_file(file_IG)
    # print(IG_file_by_line)
    line = 0
    line_envar = IG_file_by_line.index('ENV_VAR')
    # print(line_envar)
    line_var = IG_file_by_line.index('VAR')
    line_reg = IG_file_by_line.index('REG')
    while line < len(IG_file_by_line):
        if IG_file_by_line[line]:
            # print(IG_file_by_line[line])
            line_IG_wo_space = IG_file_by_line[line].replace(" ", "")
            if line_envar < line < line_var and '=' in line_IG_wo_space:
                envar = line_IG_wo_space.split("=")
                l_envar.append(envar[0])
            elif line_var < line < line_reg and line_IG_wo_space != [] and '#' not in line_IG_wo_space:
                l_Var.append(line_IG_wo_space)
            elif line_reg < line and line_IG_wo_space != [] and '#' not in line_IG_wo_space:
                l_Reg.append(line_IG_wo_space)
        line += 1
    d_IG['var'] = l_Var
    d_IG['reg'] = l_Reg
    d_IG['reg'].remove('END')
    d_IG['envar'] = l_envar
    # print(d_IG)
    return d_IG


def d_ig_creation(fich_var, fich_reg):
    d_ig = {}
    d_ig["var"] = ps_data_storage(fich_var)
    d_ig["reg"] = ps_data_storage(fich_reg)
    return (d_ig)


def ps_data_storage(PARA):
    """
    store all parameters setting provided in str list wt smb format
    :param PARA: file in smb containing PARA
    :return: l_para: list of all parameter settings instenciated
    """
    # print(PARA)
    l_Para = read_file(PARA)  # type: List[str]
    return l_Para


def vpm_data_storage(CSV, sep=","):
    """
    it takes V-PM matrix in CSV wt seprarator ','
    :param CSV: CSV file V-PM
    :return: d_PM : dict of dict seprating phenotype and biological context.
    each dict has as key  the line number of the PM and the containing line
    """
    # print(CSV,"\nfile path for matrix")
    vpm_by_line = read_file(CSV)  # type:List[str]]
    vpm_by_cell = []  # type:List[list[str]]
    for line in vpm_by_line:
        line_list = line.split(sep)  # split by "," if this with se
        vpm_by_cell.append(line_list)
    type = vpm_by_cell[0]
    ind = type.index('phenotype')  # ici voir l'écriture phenotype
    d_p = {}
    d_c = {}
    # attention commence a -1 pour la ligne des types de contexte
    line = 1
    while line < len(vpm_by_cell):
        # print(vpm_by_cell[line][0:ind])
        d_c[line - 2] = vpm_by_cell[line][0:ind]  # todo: verif bon comptage col et line
        d_p[line - 2] = vpm_by_cell[line][ind:]
        line += 1
    del d_p[-1]
    d_PM = {"context": d_c, "phenotype": d_p}  # type: Dict[str, Dict[int, list[str]]]
    return d_PM


# function  with new entery where in the context it persist only two type of  variables.  the envar variables and the systemic var which can be fixexed, or init.
# the difference is that  the nature of the systemic variable is specified in cell of the matrix  and not in header of the matrix.
def b_context_EnVar_sep_sys(d_context):
    """
    function which split into two dict the validation matrix. one dit for systemic variables and another for  envar
    variable in the biological context bloc
    :param: dict of line list
    :returns: dict of system context variable and envar context variable and their valuer ranked in dict(s) of line list
              of the context part of the VPM matrix
    """
    l_neg_1 = d_context[-1]
    del d_context[-1]
    d_SYS = {}  # type: Dict[int, list[str]]
    d_EnVar = {}  # type: Dict[int, list[str]]
    ind_SYS = -1
    if 'SYS' in l_neg_1:
        ind_SYS = l_neg_1.index('SYS')
    # print("aaaaa:"+str(ind_SYS))
    for line in d_context:
        if ind_SYS == -1:
            d_EnVar[line] = d_context[line][0:]
        else:
            d_EnVar[line] = d_context[line][0:ind_SYS]
            d_SYS[line] = d_context[line][ind_SYS:]
    return d_EnVar, d_SYS


def b_sys_separation(d_sys):
    """
     context contain envar and fix var and init var that are not handled  the same,
     it split line biological context into fixed var dict and envar dict
    :param: dict of line list of systemic vairable in the coontextpart matrix
    :returns: dict of fixed var and dict of init var  : dict(s) of line list of the matrix
    """
    # print("aaaa")
    # print(d_sys)
    d_InitVar = {0: d_sys[0]}
    d_FixVar = {0: d_sys[0]}
    i = 1
    # TODO: ajouter a cette focntion possibilité de mettre des bornes donc nouvelle façon de dire les choses osc? nouvelle catégorie
    while i < len(d_sys):
        line = d_sys[i]
        # print(line)
        l_line_init = []
        l_line_fix = []
        for var in line:
            # print(var)
            if "F" in var:
                if ".." in var:  # TODO:penser a gerer aussi les paramètres qui sont pas ...
                    l_line_fix.append(var[:-2])
                else:
                    l_line_init.append(var[:-2])
                    l_line_fix.append(var[:-2])
            elif "v" in var:
                l_line_init.append("v")
                l_line_fix.append("v")
            else:
                l_line_init.append(var)
                l_line_fix.append("v")
            d_InitVar[i] = l_line_init
            d_FixVar[i] = l_line_fix
        i += 1
    return d_InitVar, d_FixVar


def b_context_separation(d_context):
    # attention fonction que si ordre c'est env init -fix et pas de mélange.
    """
     context contain envar and fix var and init var that are not handled  the same,
     it split line biological context into three dictionary  d_FixVar, d_EnVar, d_InitVar depending on
      the type of context variable
    :param: dict of line list
    :returns:  three dict fo each kind of type of context variable filled according lines
    """
    d_InitVar = {}  # type: Dict[int, list[str]]
    d_FixVar = {}  # type: Dict[int, list[str]]
    d_EnVar = {}  # type: Dict[int, list[str]]
    l_neg_1 = d_context[-1]
    # del d_context[-1]
    # lit la première ligne et sépare les env des autres par les numéro de collones==> met une étiquette puis remplit les structures de donnée
    ind_init = -1
    ind_fix = -1
    if 'Init' in l_neg_1:
        ind_init = l_neg_1.index('Init')
    if 'Fix' in l_neg_1:
        ind_fix = l_neg_1.index('Fix')
    for line in d_context:
        if ind_fix == -1 and ind_init == -1:  # conditions for different types of vairable
            d_EnVar[line] = d_context[line][0:]
        elif ind_fix == -1 and ind_init != -1:
            d_EnVar[line] = d_context[line][0:ind_init]
            d_InitVar[line] = d_context[line][ind_init:]
        elif ind_fix != -1 and ind_init == -1:
            d_EnVar[line] = d_context[line][0:ind_fix]
            d_InitVar[line] = d_context[line][ind_fix:]
            d_FixVar[line] = d_context[line][ind_fix:]
        else:
            d_EnVar[line] = d_context[line][0:ind_init]
            d_InitVar[line] = d_context[line][ind_init:]
            d_FixVar[line] = d_context[line][ind_fix:]
        # print(d_FixVar)
    return d_FixVar, d_EnVar, d_InitVar


def b_phenotype_separation(d_phenotype):
    d_FAIRCTL = {}
    d_CTL = {}

    return d_FAIRCTL, d_CTL
# se fixer sur présence ou non des lignes avec liste vide... revoir apr raport structure d'avant


###############----------------tests-------------------############

# d_ig = ig_data_storage("IG.graphml")
# print(d_ig)


# ok

# VPM = vpm_data_storage("./../modelingcontext/V_PM.csv", "V-PM")
# print(VPM)
# d_context = VPM['context']
# print(d_context)
# d_fix, d_envar, d_init = b_context_separation(d_context)
# print('test1')
# print(d_fix,d_envar,d_init)

# VPM2 = vpm_data_storage("./../modelingcontext/V_PM2.csv", "V-PM")
# print(VPM2)
# d_context2 = VPM2['context']
# print(d_context2)
# d_fix2, d_envar2, d_init2 = b_context_separation(d_context2)
# print('test12')
# print(d_fix2,d_envar2,d_init2)
# d_envar,d_sys = b_context_EnVar_sep_sys(VPM2['context'])
# d_Init, d_fix =b_sys_separation(d_sys)
# print(d_envar,d_sys,d_Init, d_fix)
# print(d_envar2)
# print(d_init2)
# ok TEST 1 ET 2
# RESTE TESter convert run_totembionet...
